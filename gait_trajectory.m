Tk = 100;
zk = 50;
V = 1;
t = 0:Tk/2-1;
y = V*Tk/2*(1-cos(pi*t/(Tk/2)));
z = zk *(1-cos(2* pi*t/(Tk/2)));
plot(t,y)
grid;
xlabel('t[s]')
ylabel('x[mm]')
filename = 'x,png';
print('-dpng', filename);


plot(t,z)
grid;
xlabel('t[s]')
ylabel('z[mm]')
filename = 'z,png';
print('-dpng', filename);
