\chapter{Chód robota}
\label{ch:chod}

\section{Wstęp}
\label{sec:chod_wstep}
Kompleksowe rozwiązanie kinematyki odwrotnej robota kroczącego, rozumiane jako rozwiązanie dla pozycji pojedynczych nóg jak i całej platformy, stanowi podstawę do opracowania algorytmu chodu.
Podstawowym zadaniem algorytmu chodu jest generowanie trajektorii końców nóg robota, takich, aby osiągnąć kroczenie z żądanymi parametrami. Można wyróżnić następujące parametry chodu \cite{zielinska}:
\begin{description}
 \item [Okres chodu] - czas realizacji pełnej sekwencji przestawiania nóg robota
 \item [Współczynnik obciążenia] - stosunek czasu styku nogi z podłożem do okresu chodu robota
 \item [Faza względna] - znormalizowany względem okresu chodu czas od początku sekwencji kroku do danej chwili tej sekwencji
\end{description}

\section{Podział chodów}
\label{sec:przeglad}
Dla każdego rodzaju chodu robota można wyróżnić dwie fazy: przenoszenia (ang. \textit{swing phase}) oraz podparcia (ang \textit{support phase}). W fazie przenoszenia odpowiednia noga jest unoszona do góry i przemieszczana 
w kierunku ruchu robota. W fazie podparcia noga dotyka podłoża i podpiera korpus robota, przemieszczając go w kierunku ruchu. Nogi w fazie podparcia napędzają ruch robota. Kolejność, w jakiej
poszczególne nogi robota przyjmują fazy podparcia i przenoszenia określa rodzaj chodu \cite{zielinska, life}. 

Podstawowym podziałem chodów maszyn kroczących jest podział na chód swobodny (ang. \textit{free gait}) i chód periodyczny (ang. \textit{periodic gait}).
Chód swobodny charakteryzuje się brakiem ustalonej sekwencji przestawiania nóg. Kolejne ruchy są planowane na bieżąco - na podstawie sygnałów z sensorów układ sterowania wybiera którą nogę i
w jaki sposób należy przestawić, aby osiągnąć stabliny chód. Z tej przyczyny chodu swobodnego używa się, kiedy robot porusza się w nieznanym środowisku o nierównym podłożu \cite{piatek}.

W chodzie periodycznym sekwencje przemieszczania nóg robota są ustalone i wykonywane cyklicznie, w ustalonej kolejności. Można wyróżnić trzy podstawowe rodzaje chodu periodycznego \cite{piatek}:
\begin{itemize}
 \item chód metachroniczny (inaczej pełzający)
 \item chód czterotaktowy 
 \item chód trójpodporowy
\end{itemize}


\begin{figure}[H]
 \centering
 \includegraphics[width=0.8\textwidth]{img/bullet_nogi.eps}
 \caption{Oznaczenia nóg robota przyjęte na diagramach chodu}
 \label{fig:oznaczenia_nog}
\end{figure}

\subsection{Chód metachroniczny}
\label{subsec:metachroniczny}


\begin{figure}[]
 \centering
 \includegraphics[width=0.7\textwidth]{img/metachronal_gait.eps}
 \caption[Diagram chodu metachronicznego.]{Diagram chodu metachronicznego. Szare prostokąty oznaczają fazę przenoszenia.}
 \label{fig:metachroniczny}
\end{figure}

Chód metachroniczny, inaczej pełzający, bywa także nazywany chodem pięciopodporowym. Nazwa ta wynika z faktu, że w każdej chwili chodu pięc nóg znajduje się w fazie podparcia, a jedna w fazie przenoszenia.
Innymi słowy, wspołczynnik obiążenia nogi jest równy 5/6. Diagram chodu został przedstawiony na rys. \ref{fig:metachroniczny}. Ukazuje on które nogi znajdują się w fazie przenoszenia w danym
momencie chodu. Schemat oznaczeń nóg przedstawiono na rys. \ref{fig:oznaczenia_nog}.

Jest to najwolniejszy rodziaj chodu i zarazem najbardziej stabilny, dzięki podparciu pięcioma nogami jednocześnie. Przykładowym zastosowaniem tego chodu może być poruszanie się w nieznanym terenie,
gdzie można się spodziewać przeszkód \cite{piatek}.

\subsection{Chód czterotaktowy}
\label{subsec:czterotakt}
\begin{figure}[]
 \centering
 \includegraphics[width=0.7\textwidth]{img/ripple_gait.eps}
 \caption[Diagram chodu czterotaktowego.]{Diagram chodu czterotaktowego. Szare prostokąty oznaczają fazę przenoszenia.}
 \label{fig:czterotakt}
\end{figure}
Chód czterotaktowy (w literaturze anglojęzycznej \textit{ripple gait}) charakteryzuje się częściowym nałożeniem fal przeniesień nóg po prawej i lewej stronie robota (rys. \ref{fig:czterotakt}).
Skutkuje to jednoczesnym przenoszeniem dwóch nóg w pewnych fazach chodu. Pozwala to na osiągnięcie większej prędkości chodu przy zachowaniu dobrej stabilności \cite{piatek}.

\subsection{Chód trójpodporowy}
\label{subsec:tripod}
\begin{figure}[]
 \centering
 \includegraphics[width=0.7\textwidth]{img/tripod_gait.eps}
 \caption[Diagram chodu trójpodporowego.]{Diagram chodu trójpodporowego. Szare prostokąty oznaczają fazę przenoszenia.}
 \label{fig:tripod}
\end{figure}
Chód trójpodporowy (ang. \textit{tripod gait}) jest najszybszym chodem owadów szcześcionożnych. Przenoszone są trzy nogi jednocześnie, co daje współczynnik obciążenia równy 1/2.
Jest to najmniejsza wartość współczynnika, przy której chód pozostaje statycznie stabilny, tj. zatrzymanie sekwencji ruchu w dowonym momencie nie spowoduje utraty stabilności robota. 
Łatwo zauważyć, że dalsze zmniejszanie współczynnika skutkowałoby chodem, w którym mniej niż trzy nogi jednocześnie znajdują się w fazie podporowej, co czyni stabilność statyczną niemożliwą \cite{piatek}.

\section{Trajektoria ruchu nogi robota}
\label{sec:generacja_trajektorii}
Trajektoria ruchu końca nogi robota jest generowana w funkcji czasu w kartezjańskim układzie współrzędnych związanym z układem platformy robota bądź z globalnym układem współrzędnych. 
Poniższe rozważania przeprowadzono w układzie globalnym. Analizę przeprowadzono dla chodu trójpodporowego.

Załóżmy że robot porusza się z prędkością $\overrightarrow{V}$, gdzie wektor $\overrightarrow{V}$ ma zerową składową $V_z$ (nie występuje ruch w osi pionowej robota). 
Dla nóg w fazie podporowej można napisać następujące równania:
\begin{equation}
\label{eq:chod_podporowe}
\begin{split}
 x = const 
 \\
 y = const
 \\
 z = 0 = const
\end{split}
\end{equation}

Równania dla nóg znajdujących się w fazie przenoszenia mają postać:
\begin{equation}
\label{eq:chod_przenoszace_liniowe}
\begin{split}
 x = 2V_x \cdot T_h \cdot \left({ t \over T_h }\right) 
 \\
 y = 2V_y \cdot T_h \cdot \left({ t \over T_h }\right)
 \\
 z = \left\{ \begin{array}{ll}
z_k \cdot {t \over T_p} & \textrm{gdy ${t \over T_p} \leq 0,5$}\\
z_k \cdot {t \over T_p} & \textrm{gdy ${t \over T_p} > 0,5$}
\end{array} \right.
\end{split}
\end{equation}
gdzie $V_x, V_y$ - odpowiednie składowe wektora $\overrightarrow{V}$, $t$ - czas bieżący, $T_h$ - połowa okresu chodu, $z_k$ - wysokość kroku (wysokość na jaką podnoszona jest końcówka nogi
w najwyższym punkcie fazy przenoszenia).

Powyższe równania pokazują najprostszy opis trajektorii, posługujący się funkcjami liniowymi. Wadą tego rozwiązania jest występowanie skoków prędkości w połowie okresu kroku - gdy nogi zmieniają
fazę kroku, oraz w połowie okresu półkroku, gdzie noga w fazie przenoszenia osiąga punkt szczytowy i rozpoczyna opadanie. Skokom prędkości towarzyszą impulsy przyspieszenia. Jest to zjawisko niekorzystne
ze względu na gwałtowne przeciążenia w układach napędowych, mogące w skrajnych przypadkach prowadzić do przegrzewania i uszkodzenia silników serwonapędów, a także powodujące szarpanie i drgania robota \cite{zielinska}. 
Aby wyeliminować ten problem, należy opisać trajektorię zależnością funkcyjną różniczkowalną w całej dziedzinie (dzięki czemu nie będzie skoków prędkości i impulsów przyspieszenia). Do tego celu
można wykorzystać funkcje wielomianowe trzeciego stopnia bądź funkcje trygonometryczne \cite{life}.
\pagebreak
Równania opisujące trajektorię w fazie przenoszenia ,,wygładzoną'' za pomocą funkcji postaci $1-\cos t$:
\begin{equation}
\label{eq:chod_przenoszace}
\begin{split}
 x = 2V_x \cdot T_h \cdot ( 1 - cos{\left({\pi t}\over{T_p}\right)}
 \\
 y = 2V_y \cdot T_h \cdot ( 1 - cos{\left({\pi t}\over{T_p}\right)}
 \\
 z = z_k \cdot ( 1 - cos{\left({2\pi t}\over{T_p}\right)}
\end{split}
\end{equation}

Rys. \ref{fig:traj_x} i \ref{fig:traj_z} ukazują wykresy teoretycznych trajektorii generowanych z użyciem funkcji postaci $1-\cos t$ dla $V_x = 1 [mm/s], T = 100 [s], z_k = 100 [mm]$

\begin{figure}[H]
 \centering
 \includegraphics[width=0.8\textwidth]{img/trajektoria_x.png}
 \caption{Wykres współrzędnej x końca nogi będącej w fazie przenoszenia}
 \label{fig:traj_x}
\end{figure}
\begin{figure}[H]
 \centering
 \includegraphics[width=0.8\textwidth]{img/trajektoria_z.png}
 \caption{Wykres współrzędnej z końca nogi będącej w fazie przenoszenia}
 \label{fig:traj_z}
\end{figure}
\begin{samepage}
Przeprowadzona analiza i wyznaczone zależności posłużyły wraz z rozwiązaniem kinematyki odwrotnej do zaimplementowania generacji chodu robota w języku programowania, co zostało opisane w dalszej części pracy.
\end{samepage}