\chapter{Kinematyka robota}
\label{ch:kinematyka}

\section{Wstęp}
\label{sec:wstep}
Do przemieszczania robota, w szczególności do chodu, konieczne jest zadawanie odpowienich położeń serwonapędów robota. W tym celu należy rozwiązać zadanie odwrotne kinematyki mechanizmu robota. Jest to problem złożony - po pierwsze konieczne jest rozwiązanie kinematyki każdej z nóg robota, które można traktować jako mechanizmy o strukturze $\{ C_R, B_R, B_R \}$, oraz rozwiązanie kinematyki odwrotnej dla całego mechanizmu robota \cite{olszewski}. Podsumowując, zadanie kinematyki odwrodnej dla robota kroczącego sześcionożnego polega na wyznaczeniu współrzędnych maszynowych każdej z nóg robota tak, aby końce nóg robota oraz platforma osiągnęły żądaną pozycję i orientację w globalnym układzie współrzędnych $\mathbf{O}$ (rys. \ref{fig:uw}).

Rozwiązanie zadania prostego kinematyki manipulatora w odniesieniu do położenia polega na wyznaczeniu związku pomiędzy współrzędnymi maszynowymi robota a położeniem wybranego punktu mechanizmu w globalnym układzie współrzędnych. Rozwiązanie tego zadania jest trywialne, o ile znana jest struktura kinematyczna robota i jego parametry geometryczne. Możliwe jest rozpatrywanie tego zadania także w odniesieniu do prędkości, przyspieszenia oraz siły i momentu.  

Rozwiązanie zadania odwrotnego kinematyki manipulatora w odniesieniu do położenia polega na wyznaczeniu współrzędnych maszynowych robota na podstawie położenia danego punktu manipulatora, znając strukturę kinematyczną oraz geometrię robota. Podstawowym problemem w rozwiązaniu tego zadania jest niejednoznaczność rozwiązania, tj. dla jednego położenia punktu końcowego manipulatora możliwe są różne ułożenia jego członów. Można zauważyć, że dla rozpatrywanego mechanizmu dla danego punktu mogą istnieć co najwyżej dwa rozwiązania zadania kinematyki odwrotnej. 

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/kinematyka_robot_caly.eps}
\caption{Przyjęte układy współrzędnych}
\label{fig:uw}
\end{figure}

\section{Układ współrzędnych}
\label{sec:uw}
Do wyrażenia przekształceń układu współrzędnych wygodnie jest zastosować pojęcia macierzy rotacji, wektora translacji oraz jednorodnej macierzy transformacji \cite{olszewski}.

Macierz rotacji opisuje dowolny obrót w przestrzeni euklidesowej. Może być ona rozumiana jako macierz obrotu z układu $i$ do układu $j$:
\begin{equation}
 \mathbf{R_{i}^{j}} = \left[ \begin{array}{ccc}
                      a_{11}	&	a_{12}	&	a_{13}	\\
                      a_{21}	&	a_{22}	&	a_{23}	\\
                      a_{31}	&	a_{32}	&	a_{33}	\\
                     \end{array}
	      \right]
\end{equation}
gdzie $a_{kl}$ są współczynnikami macierzy obrotu powstałymi np. poprzez złożenie macierzy obrotu wokół osi układu współrzędnych.

Wektor translacji opisuje przesunięcie z układu współrzędnych $i$ do układu $j$ i ma postać:
\begin{equation}
 \mathbf{r_{i}^j}^T = \left[ \begin{array}{ccc}
                            r_x		&	r_y	&r_z
                           \end{array} \right]^T
\end{equation}

Jednorodna macierz transformacji stanowi złożenie powyższych przekształceń. Pozwala ona na wygodne zapisywanie transformacji złożonych z obrotu i przesunięcia. Możliwe jest składanie macierzy transformacji. Jednorodna macierz tranformacji ma postać:
\begin{equation}
  \mathbf{T}_{i}^{j} = \left[ \begin{array}{cc}
                      \mathbf{R}_{i}^{j}		&		\mathbf{r}_{i}^{j}\\
                     0				&		1	\\
                      
                     \end{array}
	      \right]
\end{equation}


Należy rozważyć dwa układy współrzędnych: układ globalny $Oxyz$ oraz układ lokalny związany z platformą robota: $O'x'y'z'$ (rys. \ref{fig:luw}, w dalszej części pracy układy oznaczane są jako odpowiednio $\mathbf{O}$ oraz $\mathbf{O'}$). Przyjmuje się, że układ globalny jest związany z podłożem, po którym porusza się robot, a jego położenie jest stałe w czasie. Za położenie układu $\mathbf{O}$ przyjęto położenie układu $\mathbf{O'}$ w momencie uruchomienia robota. 

%rysunek lokalnych układów współrzędnych
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/bullet_uklady_gora.eps}
\caption{Przyjęte układy współrzędnych}
\label{fig:luw}
\end{figure}

Układ platformy robota jest przesunięty względem globalnego o wektor $ \overrightarrow{OO'}$ (rys. \ref{fig:uw}), zaś zmianę jego orientacji opisuje macierz rotacji $\mathbf{R}_{O}^{O'}$:
\begin{equation}
\mathbf{R}_{O}^{O'} = \mathbf{R}_{Ox}^{O'} (\phi) \cdot \mathbf{R}_{Oy}^{O'}(\theta) \cdot \mathbf{R}_{Oz}^{O'}(\psi)
\end{equation}


gdzie ${R}_{Ox}^{O'}$, ${R}_{Oy}^{O'}$, ${R}_{Oz}^{O'}$ są macierzami obrotu wokół odpowiednich osi układu globalnego, zaś $ \phi, \theta, \psi $ są kątami tych obrotów, zwanymi kątami Eulera. W literaturze anglojęzycznej są one określanie mianem kątów ,,roll'',  ,,pitch'' oraz ,,yaw'' \cite{life, magheli}. 
Znając wypadkową macierz obrotu oraz wektor przesunięcia można wyznaczyć jednorodną macierz transformacji z układu $\mathbf{O}$ w układ $\mathbf{O'}$:


\begin{equation}
\label{eq:transformacja}
\mathbf{T}_{O}^{O'} = 
\left[ \begin{array}{ccc}
	\mathbf{R}_{O}^{O'}	& 	-\overrightarrow{OO'} \\
	0 			&	1
\end{array} \right]
\end{equation}


Punkty $L_i$ są miejscami styku nóg robota z podłożem wyrażonymi w układzie $\mathbf{O}$. Używając macierzy transformacji (\ref{eq:transformacja}) można przenieść punkty $L_i$ z układu $\mathbf{O}$ do układu $\mathbf{O'}$:

\begin{equation}
\label{eq:ltolprim}
L'_i = \mathbf{T}_{O}^{O'}  \cdot  L_i = \mathbf{T}_{O}^{O'} 
\left[ \begin{array}{ccc}
	x_{L_{i}} \\
	y_{L_{i}} \\
	z_{L_{i}}
	\end{array} \right]
\end{equation}

Zadanie kinematyki rozwiązywane jest w układzie współrzędnych $S_i x_{Si} y_{Si} z_{Si}$ związanym z początkiem łańcucha kinematycznego (w tym wypadku ,,biodrem'' robota). Układ ten w dalszej części pracy jest oznaczany $\mathbf{S}_i$, gdzie $i$ jest numerem nogi. 
Z punktu widzenia generowania trajektorii chodu potrzebne jest rozwiązanie kinematyki w układzie $\mathbf{O'}$ (platformy)  oraz $\mathbf{O}$ (globalnym) \cite{life}. 

Przejście z układu $\mathbf{O'}$ do $\mathbf{S}_i$ opisane jest przez obrót wokół osi $O'Z$ o kąt $\psi'_i$ i przesunięcie o wektor  $\overrightarrow{O'S_i}$. Wyraża to macierz transformacji:
\begin{equation}
\label{eq:t_op_to_si}
\mathbf{T}_{O'}^{S_i} = 
\left[ \begin{array}{ccc}
	\mathbf{R}_{O'}^{S_i}(\psi'_i)	& 	-\overrightarrow{O'S_i} \\
	0 			&	1
\end{array} \right]
\end{equation}

Przyjmując $L''_{i}$ jako punkt końca $i$-tej nogi robota w układzie $\mathbf{S_i}$, można napisać następującą transformację:
\begin{equation}
\label{eq:lprim_to_lbis}
 L''_i = \mathbf{T}_{O'}^{S_i} L'_i
\end{equation}

Posługując się równaniami \ref{eq:ltolprim} oraz \ref{eq:lprim_to_lbis} można przenieść punkty końców nóg robota z układu globalnego do układu platformy, a następnie do układu złączenia (biodra). Pozwala to na wygodne rozwiązanie zadania kinematyki, zaś macierzowy zapis równań umożliwia wykorzystanie zaawansowanych narzędzi matematycznych wbudowanych w biblioteki programistyczne. 
\pagebreak
\section[Zadanie odwrotne kinematyki dla pojedynczej nogi]{Zadanie odwrotne kinematyki dla pojedynczej \\ nogi}
\label{sec:kinematyka_nogi}

Jak wspomniano w podrozdziale \ref{sec:wstep}, noga robota może być rozpatrywana jako manipulator szeregowy o strukturze $\{ C_R, B_R, B_R \}$.   Aby wyznaczyć rozwiązanie zadania odwrotnego kinematyki, najpierw rozwiązano zadanie proste.  W tym celu wykorzystano notację Denavita-Hartenberga (DH) \cite{olszewski, life}. Pozwala ona na transformację układów współrzędnych za pomocą jednorodnych macierzy transformacji zbudowanych z następujących parametrów: 
\begin{itemize}
\item ${\theta}_j$ -- kąt obrotu układu $j$ względem osi $z_{j-1}$ (w tym wypadku interpretowane jako współrzędna maszynowa)
\item ${d}_j$ -- przemieszczenie początku układu $j$ w kierunku osi $z_{j-1}$ (tzw. odsadzenie członu)
\item ${a}_j$ -- przemieszczenie początku układu $j$ w kierunku osi $x_{j}$ (w tym wypadku interpretowane jako długość członu)
\item ${\alpha}_j$ -- kąt obrotu układu $j$ wokół osi $x_{j}$
\end{itemize}

Tabela \ref{tab:dh} prezentuje parametry Denavita-Hartenberga dla kolejnych punktów złączeń oznaczonych wg. rysunku \ref{fig:jp}.

\begin{table}[H]
	\begin{center}
	\begin{tabular}{|c|c|c|c|c|} \hline
	Nr. złączenia	&	${\alpha}_j$ 	&	${a}_j$	&	${d}_j$ 	&	${\theta}_j$	\\ \hline
	1			&	0			&	0		&	0		&	${\theta}_1$	\\ \hline
	2			&	$-\pi / 2$		&	$l_0$		&	0		&	${\theta}_2$	\\ \hline
	3			&	0			&	$l_1$		&	0		&	${\theta}_3$	\\ \hline
	4			&	0			&	$l_2$		&	0		&	0			\\ \hline		
	\end{tabular}
	\end{center}
	\caption{Parametry DH nogi robota \label{tab:dh}}
\end{table}

%rysunek nogi z zaznaczonymi JP i układami
\begin{figure}
\centering
\includegraphics[width=0.95\textwidth]{img/noga_uklady}
\caption{Punkty złączeń robota}
\label{fig:jp}
\end{figure}

Macierz transformacji z układu $j$ do układu $j-1$ jest wynikiem czterech kolejnych transformacji elementarnych \cite{olszewski}:
\begin{equation}
\label{eq:dh_transform_general}
\mathbf{T}_{j-1}^{j} = \mathbf{Rot}_{z_{j-1}, \theta_j} \cdot  \mathbf{Trans}_{z_{j-1}, d_j} \cdot  \mathbf{Trans}_{x_{j}, a_j} \cdot   \mathbf{Rot}_{x_{j}, \alpha_j}
\end{equation}

\pagebreak

Ostateczna macierz transformacji DH z układu 0 do układu 4 ma postać:

\begin{equation}
\label{eq:dh_transform}
\mathbf{T}_{0}^{4} = 
\left[\begin{array}{cccc} 
	C_{12}C_1		&	-S_{12}C_1	&	-S_1		&	C_1(l_0 + l_1 C_2 + l_2 C_{23})	\\
	C_{12}S_1		&	-S_{12}S_1	&	C_1		&	S_1(l_0 + l_1 C_2 + l_2 C_{23})	\\
	-S_{23}		&	-C_{23}		&	0		&	-l_1 S_2 + l_2 S_{23}			\\
	0			&	0			& 	0		&	1
 \end{array} \right]
\end{equation}

%Rysunek rozwiazania kinematyki nogi
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/noga_rozwiazanie}
\caption[Geometria rozwiązania kinematyki odwrotnej robota.]{Geometria rozwiązania kinematyki odwrotnej robota. Uwaga: kąt $\theta_2$ dla obliczeń przyjęto inaczej niż na rys. \ref{fig:jp}}
\label{fig:noga_rozw}
\end{figure}

gdzie $C_1$, $C_{12}$, $S_1$, $S_{12}$,$C_{23}$, $S_{23}$ oznaczają odpowiednio cosinusy i sinusy kątów $\theta_1$, $\theta_1 + \theta_2$, $\theta_2 + \theta_3$.

Z powyższych przekształceń łatwo wyznaczyć rozwiązanie zadania odwrotnego kinematyki robota (por. rys. \ref{fig:noga_rozw}) \cite{life}. Kąt  $ \theta_1 $ można wyznaczyć bezpośrednio:

\begin{equation}	
\label{eq:fi1}
\theta_1 = arctg\frac{x_j}{y_j}
 \end{equation}
 
 \pagebreak
 W celu wyznaczenia kątów $ \theta_2 $ i $ \theta_3 $ należy wprowadzić dodatkowe oznaczenia (por. rys. \ref{fig:noga_rozw}):

\begin{equation}
d = \sqrt{(x_j - l_0 C_1)^2 + (y_j - l_0 S_1)^2 + z_j^2}
\end{equation}

\begin{equation}
\beta = \arccos \left( \frac{d^2 + l_1^2-l_2^2}{2l_1d} \right)
\end{equation}

Z powyższego można wyznaczyć kąt $ \theta_2 $:

\begin{equation}
\theta_2 = \arcsin \left( \frac{-z_j}{d} \right) - \beta
\end{equation}

Do wyznaczenia kąta $ \theta_3 $ należy wprowadzić kąty pomocnicze 
\begin{equation}
\gamma_1 = \arccos \left( \frac{l_1\ \sin \beta}{l_2} \right)
\end{equation}

\begin{equation}
\gamma_2 = \frac{\pi}{2} - \beta
\end{equation}

\begin{equation}
\theta_3 = \pi - \gamma_1 - \gamma_2
\end{equation}

Tak wyznaczone rozwiązanie kinematyki można łatwo zaimplementować w języku programowania.