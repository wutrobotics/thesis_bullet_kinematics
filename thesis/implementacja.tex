\chapter{Implementacja rozwiązania kinematyki oraz chodu robota}
\label{ch:implementacja}

\section{Organizacja oprogramowania}
\label{sec:organizacja}
W ramach przygotowania oprogramowania robota sporządzono implementację rozwiązania zadania prostego i odwrotnego kinematyki robota oraz generator chodu trójpodporowego. Posłużono się zależnościami wyznaczonymi w rozdziałach \ref{ch:kinematyka} i \ref{ch:chod}. Ponieważ w projekcie uczestniczy wiele osób i jest prawdopodobne, że oprogramowanie będzie w przyszłości rozwijane przez innych programistów, za priorytet postawiono dobrą organizację kodu programu i bieżące przygotowywanie jego dokumentacji. Oprogramowanie zostało napisane w języku C++.

Konieczność łatwego przeniesienia oprogramowania z środowiska symulacyjnego na robota wymogła modułową organizację kodu. Z tego też powodu wydzielono generator chodu i kalkulator kinematyki jako niezależny moduł motoryki. Dzięki temu można traktować go jako ,,czarną skrzynkę'', która jako dane wejściowe przyjmuje kierunek chodu oraz prędkość liniową i kątową, zaś na wyjściu generuje współrzędne maszynowe dla punktów złączeń robota. Schemat organizacji oprogramowania robota przedstawia rysunek \ref{fig:schemat_oprogramowania}. Prostokąty oznaczają procesy ROS, zaś elipsy - tematy. Kierunki strzałek oznaczają publikowanie bądź subskrybowanie danego tematu. 

\begin{figure}[hp]
 \centering
 \includegraphics[width=0.7\textwidth]{img/schemat_oprogramowania.png}
 \caption{Schemat blokowy oprogramowania robota Bullet}
 \label{fig:schemat_oprogramowania}
\end{figure}


\section{Kalkulator kinematyki}
\label{sec:solver}
Kalkulator kinematyki wykorzystuje bibliotekę KDL (ang. \textit{Kinematics/Dynamics Library}, wydaną na licencji LGPL w ramach projektu Orocos (ang. \textit{Open RObot COntrol Software}). Biblioteka ta udostępnia m. in. klasy opisujące obiekty matematyczne przydatne przy rozwiązywaniu zagadnień geometrii analitycznej, takie jak wektory (\texttt{KDL::Vector}), macierze rotacji (\texttt{KDL::Rotation}) oraz macierze transformacji (\texttt{KDL::Frame}) \cite{orocos}. Ponadto KDL udostępnia silnik do rozwiązywania kinematyki prostej i odwrotnej dowolnego łańcucha kinematycznego. Silnik ten nie został wykorzystany w projekcie, ponieważ dla tak prostego łańcucha kinematycznego jakim jest noga robota rozwiązanie analityczne jest bardziej wydajne. 

Logika kalkulatora została zawarta w klasie \texttt{Solver}. Jest ona napisana według wzorca projektowego singleton \cite{gof}. Oznacza to, że istnieje tylko jedna instancja tej klasy (utworzenie innej instancji nie jest możliwe), zaś dostęp do niej jest realizowany przez metodę statyczną:
\begin{lstlisting}[language=C++]
  static Solver* getInstance()
\end{lstlisting}
Użycie tego wzorca jest zasadne ponieważ utworzenie wielu instancji klasy \texttt{Solver} jest niekorzystne ze względu na zużycie zasobów. W przypadku, gdyby konieczne było działanie dwóch kalkulatorów jednocześnie, należy raczej uruchomić drugi proces.

Do rozwiązania zadań kinematyki potrzebny jest opis geometrii robota. Jest on dostarczany z serwera parametrów w następującej postaci:
\begin{itemize}
 \item wektor długości członów nogi
 \item wektor odległości pierwszego punktu złącznia (,,biodra'') od środka platformy
 \item kąt obrotu układu biodra względem układu platformy wokół osi Z
\end{itemize}

Dane te zostały opracowane na podstawie modelu CAD robota i są dostarczane na serwer w pliku YAML podczas wykonywania skryptu uruchomieniowego bullet{\_}kinematics.launch. Zawartość pliku YAML przedstawia listing \ref{listing:kinematic_params}
\lstinputlisting[caption={Plik konfiguracyjny opisujący geometrię robota}, captionpos=b, numbers=left, language=XML,  label=listing:kinematic_params]{listings/kinematics.yaml}

Logika rozwiązania kinematyki pojedynczej nogi została zawarta w klasie LegSolverAnalitical. Klasa ta posiada pola przechowujące długości członów nogi oraz współczynniki $\delta_1$ i $\delta_2$ do rozwikłania niejednoznaczności rozwiązania kinematyki odwrotnej. 
Klasa udostępnia następujące metody publiczne:
\begin{description}
 \item[\texttt{solveLeg()}] - rozwiązuje zadanie kinematyki odwrotnej dla zadanego punktu i zwraca rozwiązanie przez referencję do obiektu klasy \texttt{KDL::JntArray}
 \item[\texttt{solveLegFwd()}] - rozwiązuje zadanie kinematyki prostej dla zadanego w postaci obiektu \texttt{KDL::JntArray} wektora współrzędnych maszynowych
\end{description}
Błąd rozwiązania (np. gdy zadany punkt jest nieosiągalny) jest zgłaszany poprzez wyrzucenie wyjątku \texttt{std::runtime{\_}error}, zawierającego opis błędu do wyświetlenia na konsoli.

Metody klasy \texttt{Solver} pozwalają na następujące czynności:
\begin{itemize}
 \item rozwiązanie prostego zadania kinematyki
 \item rozwiązanie odwrotnego zadania kinematyki dla punktu wyrażonego w globalnym układzie współrzędnych
 \item rozwiązanie odwrotnego zadania kinematyki dla punktu w układzie współrzędnych platformy
 \item przenoszenie punktu z układu współrzędnych danego biodra do układu platformy i odwrotnie
\end{itemize}

Funkcja główna procesu najpierw pobiera z serwera odpowiednie parametry, po czym rejestruje subskryber tematu \texttt{/bullet/robot{\_}pose}. Rozwiązanie zadania kinematyki odbywa się w funcji obsługującej nadejście wiadomości. Wiadomości są publikowane do tego tematu przez proces generatora chodu (potencjalnie przez inny proces generujący trajektorię) i mają następujący format:
\begin{itemize}
 \item 6-elementowa tablica punktów geometrycznych - opisująca pozycję nogi robota
 \item punkt geometryczny opisujący pozycję platformy w globalnym układzie współrzędnych
 \item kwaternion opisujący orientację platformy w globalnym układzie współrzędnych
\end{itemize}

Możliwe jest wybranie jednej z dwóch funkcji obsługujących wiadomości - różnica między nimi polega na rozwiązywaniu kinematyki odwrotnej w układzie globalnym lub układzie platformy. Wybór odbywa się przez ustawienie parametru \texttt{kinematics{\_}mode} na serwerze na wartość odpowiednio ,,global'' lub ,,body''.

W funkcji obsługującej zawartość wiadomości z tematu \texttt{/bullet/robot{\_}pose} jest konwertowana do obiektów KDL, rozwiązywane jest zadanie kinematyki, po czym rozwiązanie jest konwertowane na wiadomość prostą zawierającą jedno pole typu Float64 i publikowane do tematu o nazwie \texttt{/bullet/leg{\_}[x]{\_}[y]} (\texttt{x} oznacza numer nogi, zaś \texttt{y} numer złączenia).

Poza główną funkcjonalnością kalkulacji kinematyki, proces udostępnia trzy serwisy:
\begin{description}
 \item [/bullet/solve{\_}fwd] - serwis pozwalająca na rozwiązanie zadania prostego kinematyki dla danego punktu
 \item [/bullet/zero{\_}joints] - serwis zerująca współrzędne maszynowe, tj. publikująca do tematów odpowiedzialnych za pozycję napędów złączeń wartość 0
 \item [/bullet/convert{\_}frame] - serwis pozwalająca na przenoszenie punktu między układami danego biodra i platformy
\end{description}
Serwisy te są przydatne, kiedy funkcjonalności kalkulatora są potrzebne w innych procesach ROS. Wszystkie zaimplementowane serwisy są wykorzystywane przez generator chodu do wygenerowania postawy bazowej, a także do wygenerowania trajektorii wstawania i kładzenia robota. 

\section{Generator chodu}
\label{sec:generator}
Podobnie jak kalkulator kinematyki, generator chodu do obliczeń wykorzystuje klasy z biblioteki KDL. Logika generacji chodu została zawarta w klasach \texttt{GaitGenerator} oraz \texttt{GaitGeneratorSimple}. Obydwie klasy zostały napisane według wzorca singleton (por. sekcja \ref{sec:solver}). Różnica w działaniu generatorów w tych klasach leży w układzie współrzędnych, w jakich generowany jest chód. W klasie \texttt{GaitGenerator} jest on generowany w globalnym układzie współrzędnych, zaś w klasie \texttt{GaitGeneratorSimple} - w układzie platformy. Jak wskazuje nazwa, drugie rozwiązanie pozwoliło na prostszą, bardziej przejrzystą i łatwiejszą do rozbudowania implementację chodu robota. Dlatego rozwój klasy \texttt{GaitGenerator} został wstrzymany - pozwala ona na generowanie chodu tylko z prędkością liniową. Ponieważ API tych klas jest niemal identyczne, możliwe jest dokończenie implementacji klasy \texttt{GaitGenerator} i stosowanie jej zamiennie z \texttt{GaitGeneratorSimple} w zależności od potrzeb.

Do generacji chodu potrzebne są następujące parametry (por. rozdział \ref{ch:chod}):
\begin{itemize}
 \item wysokość kroku ($z_k$, por. wzór \ref{eq:chod_przenoszace}) - wysokość na jaką podnoszona jest noga w fazie przenoszenia
 \item okres kroku $T_k$- czas pełnego cyklu kroku
 \item okres próbkowania $t_p$- okres generacji punktów trajektorii
 \item tzw. postawa bazowa lub baza chodu - wektor punktów reprezentujących położenie koncówek nóg robota w układzie współrzędnych platformy gdy robot stoi nieruchomo. Problem generowania bazy chodu omówiono w dalszej części rozdziału.
\end{itemize}

Powyższe parametry należy ustawić w generatorze za pomocą odpowiednich funkcji przed rozpoczęciem generowania trajektorii. Sama generacja chodu polega na wywoływaniu metody \texttt{generateTrajectory()} z interwałem $t_p$. Do tego celu wykorzystano konstrukcję programową przedstawioną na listingu \ref{listing:while}. 
\lstinputlisting[caption={Pętla główna procesu bullet{\_}gait}, captionpos=b, numbers=left, language=C++, label=listing:while]{listings/while.c} 
Jako warunku podtrzymującego pętlę użyto funkcji \texttt{ros::ok()}. Jest to funkcja przechwytująca sygnał SIGINT systemu operacyjnego, pozwalająca na opuszczenie pętli i zakończenie procesu, gdy użytkownik naciśnie kombinację klawiszy Ctrl+C \cite{roswiki}. Jako pierwsza wywoływana jest funkcja \texttt{generateTrajectory()} z argumentem typu wyliczeniowego wskazującym, że generowana ma być trajektoria chodu. W wyniku działania tej funkcji wygenerowana zostanie pozycja robota w postaci wektora punktów (\texttt{std::vector<KDL::Vector>}. Jest ona pobierana za pomocą funkcji \texttt{getRobotPose}. Następnie wykonywana jest wektora punktów na wiadomość typu RobotPose, która zostaje natychmiast opublikowana w temacie \texttt{/bullet/robot{\_}pose}. Linia 6 pozwalają ROS'owi przejąć kontrolę nad programem m. in. w celu wywołania funkcji obsługujących subskrybery, zaś w linii 7 proces jest usypiany na zadany interwał $t_p$.

Do wyliczania trajektorii generator wprowadza pomocniczą wielkość -- rozdzielczość trajektorii:
\begin{equation}
 N = {T_k \over t_p}
\end{equation}
Wykorzystywana jest także faza kroku $n$, gdzie $N>n \geq 0$.
Każde wywołanie funkcji generującej powoduje zinkrementowanie fazy $n$, przy czym:
\begin{itemize}
 \item jeśli $n=0$, czyli cykl kroku rozpoczyna się, wykonywane są czynności przygowawcze opisane dalej
 \item jeśli $n=N$, czyli cykl kroku jest kończony, faza zostaje wyzerowana
 \item dla pozostałych wartości trajektoria generowana jest normalnie
\end{itemize}

Czynności przygotowawcze na początku każdego cyklu obejmują wyznaczenie wartości zadanych położenia nóg na końcu cyklu uwzględniając prędkość liniową, kątową oraz zadany okres kroku, a także wyznaczenie odchyłki pozycji nogi względem wartości zadanej. Takie ujęcie jest wygodne przy generowaniu pozycji nóg według wzorów:
\begin{equation}
\label{eq:x_gladkie}
 x_i = x_{bi} + \Delta x_i (1- \cos(\pi {n \over N})) 
\end{equation}
\begin{equation}
\label{eq:y_gladkie}
 y_i = y_{bi} + \Delta y_i (1- \cos(\pi {n \over N}))
 \end{equation}
\begin{equation}
 z_i = Z_{Bi} + z_k (1- \cos(\pi {n \over N})) \quad \textrm{dla nogi w fazie przenoszenia} \\
 \end{equation}
\begin{equation}
 z_i = Z_{Bi} \quad \textrm{dla nogi w fazie podporowej}
\end{equation}
gdzie $x_i, y_i$ są współrzędnymi końca $i$-tej nogi w układzie platformy, $x_{bi}, y_{bi}$  są współrzędnymi nogi w pozycji bazowej cyklu (tj. pozycji nogi na początku cyklu), $\Delta x_i, \Delta y_i$ są odchyłkami współrzędnych nogi na początku cyklu, zaś $z_{Bi}$ oznacza współrzędną bazową nogi (w postawie bazowej).

Należy zaznaczyć, że generowanie trajektorii innej niż chód, np. trajektorii wstawania, przebiega bardzo podobnie. Różnica polega na wywoływaniu metody \texttt{generateTrajectory()} z argumentem wskazującym na niestandardową trajektorię. Ponadto należy sprawdzać wartość zwracaną przez funkcję generacji - jest ona typu binarnego, wartość ,,prawda'' oznacza że zadana trajektoria została wygenerowana w całości. 

Podstawową informacją wejściową dla generatora chodu jest wektor prędkości liniowej i kątowej. Ponieważ nie przewiduje się ruchu z prędkością kątową inną niż o wektorze zgodnym z osią Z platformy robota, wartość prędkości kątowej jest dostarczana jako skalar. Podobnie w wypadku prędkości liniowej - tylko współrzędne X i Y wektora są brane pod uwagę. Prędkości są ustawiane w generatorze za pomocą odpowiednich funkcji. 

Aby umożliwić sterowanie pracą generatora przez zewnętrzne procesy, prędkość jest pobierana poprzez subskrybcję odpowiedniego tematu. Prędkość jest publikowana jako wiadomość \texttt{geometry{\_}msgs/Twist}. Zawiera ona dwa wektory - prękości liniowej i kątowej. Założono, że docelowo używane będą dwa tematy, odpowiadające dwóm trybom zadawania prędkości:
\begin{itemize}
 \item \texttt{/bullet/teleop/body{\_}relative} - temat, do którego publikowana jest prędkość rozpatrywana w układzie platformy
 \item \texttt{/bullet/teleop/absolute} - temat, do którego publikowany jest wektor prędkość w układzie globalnym
\end{itemize}

Zadawanie prędkości w układzie globalnym wymagałoby przeliczania jej na układ platfomy. Do tego konieczna jest znajomość orientacji robota. Wprawdzie układ sensoryczny robota pozwala określić jego orientację, jednak to rozwiązanie nie zostało zaimplementowane i nie jest przedmiotem niniejszej pracy. Dlatego w zaimplementowanym oprogramowaniu jest możliwość sterowania prędkością w układzie platformy. 

Istotnym zagadnieniem jest generowanie bazy chodu. W tym celu zaimplementowano następujący algorytm:
\begin{enumerate}
 \item Rozwiąż zadanie proste kinematyki dla każdej nogi i współrzędnych maszynowych $\theta = [0, 0, 0]$ (rozwiązanie w układzie odpowiedniego biodra)
 \item Odejmij od współrzędnej $x$ każdego punktu stałą, parametryczną wartość $x_{p}$ - spowoduje to ,,przyciągnięcie'' nogi do korpusu
 \item Ustaw współrzędną $z$ każdego punktu na wartość $-h$, gdzie $h$ jest wysokością w osi $OZ$, na jakiej ma się znajdować platforma gdy robot stoi w postawie bazowej.
 \item Przenieś wyznaczone punkty do układu platformy (lub globalnego, jeśli używana jest klasa GaitGenerator)
\end{enumerate}

Wartości $x_p$, $z_k$ i $h$ do testów symulacyjnych dobrano eksperymentalnie. 










