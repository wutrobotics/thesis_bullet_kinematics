while(ros::ok()){
    GaitGeneratorSimple::getInstance()->generateTrajectory(TRAJECTORY_GAIT);
    GaitGeneratorSimple::getInstance()->getRobotPose(leg_tips);
    pose_msg =  convertToMessage(position, orientation, leg_tips);
    pub.publish(pose_msg);
    ros::spinOnce();
    rate.sleep();
}
